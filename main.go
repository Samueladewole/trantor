package main

import (
	log "github.com/cihub/seelog"

	"flag"
	"net/http"
	"os"

	trantor "gitlab.com/trantor/trantor/lib"
	"gitlab.com/trantor/trantor/lib/database"
	"gitlab.com/trantor/trantor/lib/storage"
)

func main() {
	var (
		httpAddr     = flag.String("addr", ":8080", "HTTP service address")
		dbAddr       = flag.String("db-addr", "localhost:5432", "IP address of the database")
		dbUser       = flag.String("db-user", "", "User name to access the database")
		dbPassword   = flag.String("db-password", "", "Password to access the database")
		dbName       = flag.String("db-name", "trantor", "Name of the database")
		storePath    = flag.String("store", "store", "Path of the books storage")
		assetsPath   = flag.String("assets", ".", "Path of the assets (templates, css, js, img)")
		loggerConfig = flag.String("logger-conf", "logger.xml", "xml configuration of the logger")
		setAdminUser = flag.String("set-admin-user", "", "create/update this user to be admin if set-admin-pass is also specified")
		setAdminPass = flag.String("set-admin-pass", "", "create/update admin user password if set-admin-user is also specified")
		ro           = flag.Bool("ro", false, "read only mode")
	)
	flag.Parse()

	defer log.Flush()
	err := trantor.UpdateLogger(*loggerConfig)
	if err != nil {
		log.Error("Error loading the logger xml: ", err)
	}
	log.Info("Start the imperial library of trantor")

	db, err := database.Init(database.Options{
		Addr:     *dbAddr,
		User:     *dbUser,
		Password: *dbPassword,
		Name:     *dbName,
	})
	if err == nil {
		if *setAdminUser != "" && *setAdminPass != "" {
			db.SetAdminUser(*setAdminUser, *setAdminPass)
		}
	}
	if err != nil {
		log.Critical("Problem initializing database: ", err)
		os.Exit(1)
	}
	defer db.Close()

	store, err := storage.Init(*storePath)
	if err != nil {
		log.Critical("Problem initializing store: ", err)
		os.Exit(1)
	}

	if *ro {
		store = storage.RO(store)
		db = database.RO(db)
	}

	template := trantor.InitTemplate(*assetsPath)
	sg := trantor.InitStats(db, store, template, *ro)
	trantor.InitUpload(db, store)
	trantor.InitTasks(db, *loggerConfig)

	router := trantor.InitRouter(db, sg, *assetsPath)
	server := http.Server{
		Addr:    *httpAddr,
		Handler: router,
	}
	log.Error(server.ListenAndServe())
}
