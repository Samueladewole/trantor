module gitlab.com/trantor/trantor

go 1.14

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/go-pg/pg/v9 v9.0.1
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/jmhodges/gocld2 v0.0.0-20180523203442-58c6ee0ad8b6
	github.com/meskio/epubgo v0.0.0-20160213181628-90dd5d78197f
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/onsi/ginkgo v1.10.3 // indirect
	github.com/onsi/gomega v1.7.1 // indirect
	github.com/prometheus/client_golang v1.2.1
	github.com/prometheus/common v0.7.0
	golang.org/x/crypto v0.0.0-20191105034135-c7e5f84aec59
)
